const cardInfo = document.getElementById("cardInfo");
const cardList = document.getElementById("cardList");
const getCardDetails = document.getElementById("getCardDetails");
const checkListDiv = document.getElementById("checklistBody");

const key = "1aae3b60363dcd8244f1dc440d4a9d93";
const token =
	"7906045e527352935f4599216ef550a6295534cc9f18e007d94599f628bba230";

let listId;

const urlLink = window.location.href;
const newUrl = new URL(urlLink);

const boardId = newUrl.searchParams.get("id");
const boardName = newUrl.searchParams.get("name");

// Get the modal
let modal = document.getElementById("myModal");

// Get the button that opens the modal
let btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
let span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function () {
	modal.style.display = "block";
};

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
	modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
	if (event.target == modal) {
		modal.style.display = "none";
	}
};

document.getElementById("boardNameDisc").innerText = boardName;
document.addEventListener("DOMContentLoaded", () => {
	//getting list name
	cardInfo.style.display = "block";
	getBoardDetails();

	async function getBoardDetails() {
		try {
			const response = await fetch(
				`https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`,
				{
					method: "GET",
				}
			);
			const text = await response.json();
			writingListName(text);
			function writingListName(text) {
				listId = text[0].id;
				const h5Tag = document.getElementById("listName");
				const name = text[0].name;
				h5Tag.innerText = name;
				gettingCardDetails();
				// get cards
				async function gettingCardDetails() {
					try {
						const response = await fetch(
							`https://api.trello.com/1/lists/${listId}/cards?key=${key}&token=${token}`,
							{
								method: "GET",
							}
						);
						const text = await response.json();

						passingDetails(text);
						function passingDetails(text) {
							text.forEach((element) => {
								getCard(element);
							});
						}
					} catch (err) {
						console.log(err);
					}
				}
			}
		} catch (err) {
			console.error(err);
		}
	}
});

//function to get all cards in a list
function getCard(element) {
	const newDiv = document.createElement("div");
	newDiv.innerHTML = element.name;
	const button = document.createElement("button");
	button.innerText = "Delete";
	button.id = element.id;
	button.classList.add("deleteBtn");
	button.addEventListener("click", (event) => {
		fetch(
			`https://api.trello.com/1/cards/${button.id}?key=${key}&token=${token}`,
			{ method: "DELETE" }
		)
			.then(() => {
				event.target.parentElement.remove();
			})
			.catch((err) => console.error(err));
	});
	//OnClick fxn on each Card.
	newDiv.addEventListener("click", (event) => {
		if (event.target !== button) {
			document.getElementById("popUp").style.display = "block";
			checkListData();
		}

		//API Get for creating checkList.
		async function checkListData() {
			const response = await fetch(
				`https://api.trello.com/1/cards/${button.id}/checklists?key=${key}&token=${token}`,
				{
					method: "GET",
				}
			);
			const data = await response.json();

			data.forEach((ele) => {
				createCheckList(ele);
			});
		}
		//Crating checkList items
		function createCheckList(text) {
			const newDiv = document.createElement("div");
			newDiv.innerHTML = `<div id="checklistLogo">
				<div>
					<i class="far fa-check-square"></i>
					<span id="checkListTitle">${text.name}</span>
				</div>
			</div>

			<div>
			<button id="addItemList" idName=${text.id} name=${text.name}>Add an item</button>
			</div>

			<div id="checklistDelete">
				<button id=${text.id} class="deleteChecklist" name=${text.name}>Delete</button>
			</div>`;

			checkListDiv.appendChild(newDiv);
			//delete CheckList
			// const deleteCheck = document.querySelector(".deleteChecklist");
			// console.log(deleteCheck);
		}

		//delete CheckList
		// const deleteCheck = document.querySelector(".deleteChecklist");
		// console.log(deleteCheck);
		// deleteCheck.addEventListener("click", (e) => {
		// 	const id = e.target.id;
		// 	//console.log(id);
		// 	fetch(`https://api.trello.com/1/checklists/${id}`, {
		// 		method: "DELETE",
		// 	})
		// 		.then((response) => {
		// 			console.log(`Response: ${response.status} ${response.statusText}`);
		// 			return response.text();
		// 		})
		// 		.then((text) => console.log(text))
		// 		.catch((err) => console.error(err));
		// });

		//console.log("Hi");
		const popUpName = document.querySelector(".CardNameInfo");

		const cardName = event.target.innerText.split("\n")[0];
		popUpName.innerText = cardName;

		/***CheckList */
		const checkListBtn = document.getElementById("myBtnAdd");
		const textTitle = document.getElementById("checklistTitle");
		checkListBtn.addEventListener("click", () => {
			const name = textTitle.value;
			fetch(
				`https://api.trello.com/1/cards/${button.id}/checklists?key=${key}&token=${token}&name=${name}`,
				{
					method: "POST",
				}
			)
				.then((response) => {
					//console.log(`Response: ${response.status} ${response.statusText}`);
					return response.text();
				})
				.then((text) => {
					createCheckList(text);
				})
				.catch((err) => console.error(err));
		});
	});

	newDiv.append(button);
	newDiv.classList.add("cardDesign");
	cardList.insertBefore(newDiv, cardInfo);
}
const closeBtn = document.getElementsByClassName("closeBtn")[0];
closeBtn.addEventListener("click", () => {
	document.getElementById("popUp").style.display = "none";
});
function convertToCard() {
	const newDiv = document.createElement("div");
	newDiv.innerHTML = cardInfo.value;
	const button = document.createElement("button");
	button.innerText = "Delete";
	button.classList.add("delete");
	button.id = "axc" + Math.random() * 100 + "idx";
	location.reload();
	button.addEventListener("click", (event) => {
		fetch(
			`https://api.trello.com/1/cards/${button.id}?key=${key}&token=${token}`,
			{ method: "DELETE" }
		)
			.then(() => {
				event.target.parentElement.remove();
			})
			.catch((err) => console.error(err));
	});
	newDiv.append(button);
	newDiv.classList.add("cardDesign");
	cardList.insertBefore(newDiv, cardInfo);

	fetch(
		`https://api.trello.com/1/cards?key=${key}&token=${token}&idList=${listId}&name=${cardInfo.value}`,
		{ method: "POST" }
	)
		.then(console.log("post success"))
		.catch((err) => console.error(err));
}

getCardDetails.addEventListener("click", () => {
	convertToCard();
});

/**
 * Load ui when DOM loaded
Your ui changes should update to DOM
Use response from api to update your UI

 */
