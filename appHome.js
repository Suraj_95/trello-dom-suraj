let modal = document.getElementById("myModal");

// Get the button that opens the modal
let btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
let span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function () {
	modal.style.display = "block";
};

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
	modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
	if (event.target == modal) {
		modal.style.display = "none";
	}
};

const key = "1aae3b60363dcd8244f1dc440d4a9d93";
const token =
	"7906045e527352935f4599216ef550a6295534cc9f18e007d94599f628bba230";
const button = document.getElementById("myModalBtn");
function eventBoardCreation(url) {
	fetch(url, {
		method: "POST",
	})
		.then((response) => {
			console.log(`Response: ${response.status} ${response.statusText}`);
			return response.json();
		})
		.then((text) => createBoard(text.name, text.id))
		.catch((err) => console.error(err));
}

button.addEventListener("click", () => {
	const boardName = document.getElementById("textDisc").value;
	const url = `https://api.trello.com/1/boards/?key=${key}&token=${token}&name=${boardName}`;
	eventBoardCreation(url);
});

function allBoardDetails(key, token) {
	const url = `https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`;
	fetch(url)
		.then((res) => res.json())
		.then((res) => {
			res.forEach((element) => {
				const boardName = element.name;
				const boardId = element.id;
				createBoard(boardName, boardId);
			});
		});
}
allBoardDetails(key, token);
function createBoard(boardName, id) {
	//console.log(boardName, id);
	let newItem = document.createElement("div");
	newItem.classList.add("boardData");
	newItem.id = id;
	let h3tag = document.createElement("h3");
	h3tag.innerText = boardName;
	newItem.appendChild(h3tag);
	//console.log(newItem);
	newItem.addEventListener("click", () => {
		location.href = `index.html?id=${id}&name=${boardName}`;
	});

	let list = document.querySelector(".boardInfo");
	let addButton = document.getElementById("myBtn");
	list.insertBefore(newItem, addButton);
}
